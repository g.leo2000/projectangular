import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userId: string = "Rino";
  password: string = "";

  autenticato: boolean = true;
  errMsg: string = "Autenticazione fallita";

  constructor(private route: Router) { }

  ngOnInit(): void {
  }

  gestAuth = () : void => {
    if (this.userId === "Rino" && this.password === "test") {
      //this.autenticato = true;
      this.route.navigate(['welcome',this.userId]);
    } else {
      this.autenticato = false;
    }
  }

}
